package plugins

import (
	"encoding/gob"
	"math/rand"
	"os"
	"strconv"
)

var numquotes int
var quoteids = make(map[int]string)
var creatornames = make(map[int]string)
var nameid = make(map[string][]int)
var idquote = make(map[int]string)
var files = map[string]interface{}{"quotes/numquotes.gob": &numquotes, "quotes/quoteids.gob": &quoteids, "quotes/creatornames.gob": &creatornames, "quotes/nameid.gob": &nameid, "quotes/idquote.gob": &idquote}

func WriteQuote(key string, value string, creator string) {
	var i int
	numquotes++
	i = numquotes
	quoteids[i] = key
	creatornames[i] = creator

	nameid[key] = append(nameid[key], i)
	idquote[i] = value
	SaveData()
}

func ReadQuote(key string) (string, int) {
	qlen := len(nameid[key])

	if qlen == 0 {
		return "", 0
	}
	chosenq := rand.Intn(qlen)
	id := nameid[key][chosenq]

	text := idquote[id]
	return text, id
}

func ReadQuoteID(id string) (string, int, string, string) {
	rid, err := strconv.Atoi(id)
	if err != nil {
		return "Not a Number.", 0, "", ""
	}
	if idquote[rid] != "" {
		text := idquote[rid]
		qname := quoteids[rid]
		creator := "null"
		if creatornames[rid] != "" {
			creator = creatornames[rid]
		}
		return text, rid, qname, creator
	}
	return "No quotes with that ID found.", 0, "", ""
}

func RemoveQuote(id string) string {
	rid, err := strconv.Atoi(id)
	if err != nil {
		return "Not a Number."
	}

	if idquote[rid] != "" {
		qname := quoteids[rid]

		delete(quoteids, rid)
		delete(nameid, qname)
		delete(idquote, rid)
		SaveData()
		return "Quote removed."
	}
	return "No quotes with that ID found."
}

func SaveData() {
	os.Mkdir("quotes", 0755)
	for file, data := range files {
		encodeFile, err := os.Create(file)
		if err != nil {
			panic(err)
		}
		encoder := gob.NewEncoder(encodeFile)
		if err := encoder.Encode(data); err != nil {
			panic(err)
		}
		encodeFile.Close()
	}
}

func LoadData() {
	os.Mkdir("quotes", 0755)
	for file, data := range files {
		decodeFile, err := os.OpenFile(file, os.O_CREATE, 0666)
		if err != nil {
			panic(err)
		}
		decoder := gob.NewDecoder(decodeFile)
		decoder.Decode(data)
		decodeFile.Close()
	}
}
