package plugins

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	toml "github.com/pelletier/go-toml"
)

func Commands(d *discordgo.Session, m *discordgo.MessageCreate, conf *toml.Tree, trustedusers map[string]bool) {
	message := m.Content
	command := strings.ToLower(message)

	//Only run these commands if user trusted
	if trustedusers[m.Author.ID] {

		switch {

		//If message starts with >say, say the following text
		case strings.HasPrefix(command, ">say "):
			{
				s := strings.SplitN(message, " ", 2)
				fmt.Println(s)
				d.ChannelMessageSend(m.ChannelID, s[1])
			}

		//If message starts with >del, say the following text
		case strings.HasPrefix(command, ">del "), strings.HasPrefix(command, ">rm "):
			{
				s := strings.Split(message, " ")
				fmt.Println(s)
				var err error
				if len(s) == 2 {
					err = d.ChannelMessageDelete(m.ChannelID, s[1])
				} else if len(s) == 3 {
					err = d.ChannelMessageDelete(s[1], s[2])
				}
				if err != nil {
					d.ChannelMessageSend(m.ChannelID, err.Error())
				}
			}

		//If message starts with >search, google the following text
		case strings.HasPrefix(command, ">search "), strings.HasPrefix(command, ">google "):
			{
				s := strings.SplitN(message, " ", 2)
				fmt.Println(s)
				go SearchGoogle(s[1], 10, conf.Get("googleapi").(string), conf.Get("engineid").(string), d, m)
			}

		//If message starts with >images, image search the following text
		case strings.HasPrefix(command, ">images "), strings.HasPrefix(command, ">image "):
			{
				s := strings.SplitN(message, " ", 2)
				fmt.Println(s)
				go SearchImages(s[1], 10, conf.Get("googleapi").(string), conf.Get("engineid").(string), d, m)
			}

		//If message starts with >yt, youtube search the following text
		case strings.HasPrefix(command, ">yt "), strings.HasPrefix(command, ">youtube "):
			{
				s := strings.SplitN(message, " ", 2)
				fmt.Println(s)
				go SearchYoutube(s[1], 10, conf.Get("youtubeapi").(string), d, m)
			}

		//If message starts with >pfp set avatar to attached image
		case strings.HasPrefix(command, ">pfp"), strings.HasPrefix(command, ">avatar"):
			{
				img := m.Message.Attachments[0].URL

				baseimg := EncodeImage(img)

				conf.Set("image", baseimg)
				_, err := d.UserUpdate("", "", "", baseimg, "")
				if err != nil {
					fmt.Println(err)
					d.ChannelMessageSend(m.ChannelID, err.Error())
				}
			}

		//If message starts with >game, set status to playing game
		case strings.HasPrefix(command, ">game "), strings.HasPrefix(command, ">status "):
			{
				s := strings.SplitN(message, " ", 2)
				fmt.Println(s)
				conf.Set("status", s[1])
				d.UpdateStatus(0, s[1])
			}

		//If message starts with >trust, add id to trusted listt
		case strings.HasPrefix(command, ">trust "):
			{
				//s := strings.SplitN(m.Content, " ", 2)
				fmt.Println(message)
				user := m.Mentions[0].ID
				trustedusers[user] = true

				conf.Set("trustedusers", trustedusers)
				f, _ := os.Create("config.toml")
				f.WriteString(conf.String())
			}

		//If message starts with >untrust, remove id from trusted list
		case strings.HasPrefix(command, ">untrust "):
			{
				fmt.Println(message)
				user := m.Mentions[0].ID
				if trustedusers[user] {
					delete(trustedusers, user)

					conf.Set("trustedusers", trustedusers)
					f, _ := os.Create("config.toml")
					f.WriteString(conf.String())
				}
			}

		//If message starts with >trusted, send list of trusted ids
		case command == ">trusted":
			{
				var s string
				for user := range trustedusers {
					s = s + "<@" + user + ">, "
				}
				d.ChannelMessageSend(m.ChannelID, s)
			}

		//If message starts with >name change nick on all servers
		case strings.HasPrefix(command, ">name "), strings.HasPrefix(command, ">nick "):
			{
				s := strings.SplitN(message, " ", 2)
				fmt.Println(s)
				guilds, err := d.UserGuilds(100, "", "")
				if err != nil {
					fmt.Println(err)
					return
				}
				for _, guild := range guilds {
					conf.Set("nickname", s[1])
					d.GuildMemberNickname(guild.ID, "@me", s[1])
				}
			}
		}
	}

	switch {

	// If the message is ">git" link to gitlab
	case command == ">git", command == ">source":
		{
			d.ChannelMessageSend(m.ChannelID, "https://gitlab.com/ibrokemypie/badbot")
		}

	// If the message is ">stats" send ram and cpu usage
	case command == ">s", command == ">stats":
		{
			d.ChannelMessageSend(m.ChannelID, Stats())
		}

	// If the message is ">help" return help
	case command == ">help", command == ">h":
		{
			d.ChannelMessageSend(m.ChannelID, "https://gitlab.com/ibrokemypie/badbot/blob/master/readme.md")
		}

	// If message is ">np" send now playing spotify track
	case command == ">np", command == ">playing":
		{
			d.ChannelMessageSendEmbed(m.ChannelID, lastfmPlaying(conf.Get("lastfmapi").(string), conf.Get("lastfmuser").(string)))
		}

	// If the message is ">woof" send randoim dog"
	case command == ">woof", command == ">dog":
		{
			d.ChannelMessageSend(m.ChannelID, Woof())
		}

	// If the message is ">meow" send random cat"
	case command == ">meow", command == ">cat":
		{
			d.ChannelMessageSend(m.ChannelID, Meow())
		}

	// If the message is ">ping" reply with "Pong!"
	case command == ">ping":
		{
			d.ChannelMessageSend(m.ChannelID, "pong")
		}

	// If the message is ">pong" reply with "Ping!"
	case command == ">pong":
		{
			d.ChannelMessageSend(m.ChannelID, "ping")
		}

	// If starts with >>> add quote with given name and content
	case strings.HasPrefix(command, ">>> "):
		{
			s := strings.SplitN(message, " ", 3)
			if len(s) < 3 {
				d.ChannelMessageSend(m.ChannelID, "Usage is ``>>> name quote``")
				return
			}
			fmt.Println(s)
			s[2] = strings.Replace(s[2], "\n", "\\n", -1)
			WriteQuote(s[1], s[2], m.Author.Username+":"+m.Author.Discriminator)
			d.ChannelMessageSend(m.ChannelID, "Quote "+s[1]+" added.")
		}

	// If starts with >> get quote with given name
	case strings.HasPrefix(command, ">> "):
		{
			s := strings.SplitN(message, " ", 2)
			if len(s) != 2 {
				d.ChannelMessageSend(m.ChannelID, "Usage is ``>> quotename``")
				return
			}
			fmt.Println(s)
			q, i := ReadQuote(s[1])
			if i != 0 {
				q = strings.Replace(q, "\\n", "\n", -1)
				d.ChannelMessageSend(m.ChannelID, "``#"+strconv.Itoa(i)+"`` "+s[1]+": \n"+q)
			} else {
				d.ChannelMessageSend(m.ChannelID, "No quotes with that name found.")
			}
		}

	// Get quote with given ID
	case strings.HasPrefix(command, ">qid "), strings.HasPrefix(command, ">quoteid "):
		{
			s := strings.SplitN(message, " ", 2)
			if len(s) != 2 {
				d.ChannelMessageSend(m.ChannelID, "Usage is ``> quoteid``")
				return
			}
			fmt.Println(s)
			q, i, n, c := ReadQuoteID(s[1])
			if i != 0 {
				q = strings.Replace(q, "\\n", "\n", -1)
				d.ChannelMessageSend(m.ChannelID, "``#"+strconv.Itoa(i)+" added by "+c+"`` "+n+": \n"+q)
			} else {
				d.ChannelMessageSend(m.ChannelID, q)
			}
		}

	// Delete quote with given ID
	case strings.HasPrefix(command, ">qdel "), strings.HasPrefix(command, ">qrm "):
		{
			s := strings.SplitN(message, " ", 2)
			if len(s) != 2 {
				d.ChannelMessageSend(m.ChannelID, "Usage is ``> qdel``")
				return
			}
			fmt.Println(s)
			r := RemoveQuote(s[1])
			d.ChannelMessageSend(m.ChannelID, r)
		}
		// If the message is ">spin" send a spinner
		// if strings.HasPrefix(m.Content, ">spin ") || strings.HasPrefix(m.Content, ">spinner ") {
		// s := strings.SplitN(m.Content, " ", 2)
		// fmt.Println(s)
		//
		// n, err := strconv.Atoi(s[1])
		// if err != nil {
		// fmt.Println(err)
		// d.ChannelMessageSend(m.ChannelID, err.Error())
		// return
		// }
		//
		// Spinner(d, m, n)
		// }
	}
}
